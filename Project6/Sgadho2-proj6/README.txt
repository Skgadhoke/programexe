Simar Gadhoke

COMPILE:
- You need to be in the prog6 directory then compile all the .java files, see following
 
cd prog6/
javac *.java

RUN: 
- You need to be in the one directory above prog6 then run the file, see following

cd ../
java prog6.Sgadho2poj6


Some weird things:

There might be a weird case where it might go into my try catch and for some reason will go into the catch, and output the print statement, ERROR: exceeding the number of airports created, but it is not and adds the connection. This happens when I read it from the file, but when I just said “i 1 2” it did not happen. It happens for certain cases, but not all.